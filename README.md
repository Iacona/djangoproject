# Django Project

A simple example of using Django with its authentication system

## Installation

1) Clone the repository 
2) go inside djangoproject

```bash
sudo docker-compose build
sudo docker-compose up
```

the server is (I hope) started

## Usage

you can access the server in localhost with:

http://localhost:8000/app/connexion     -> to connect
http://localhost:8000/app/connected     -> to see the main page
http://localhost:8000/app/deconnexion   -> to deconect

there is only one user in database :

username : alexandre
password : 4444