from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm

class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)

class EditEmailForm(UserChangeForm):
    
    class Meta:
        model = User
        fields = ('email', 'password')