from django.urls import path
from . import views

urlpatterns = [
    path('connected', views.connected),
    path('connexion/', views.connexion),
    path('deconnexion/', views.deconnexion, name='deconnexion'),
]