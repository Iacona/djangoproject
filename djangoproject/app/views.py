from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserChangeForm
import json

from . import forms

# Create your views here.

def connected(request):
    if request.user.is_authenticated == False:
        return redirect(reverse(connexion))
    if request.method == 'POST':
        form = forms.EditEmailForm(request.POST, instance=request.user)
        message = 'something wrong!'
        if form.is_valid():
            form.save()
            message = request.POST.get('title', "Guest")
        #return HttpResponse(json.dumps({'message': message}))
        args = {'form': form}
        return render(request, 'loged.html', )

    else:
        form = forms.EditEmailForm(instance=request.user)
        args = {'form': form}
        return render(request, 'loged.html', args)

def connexion(request):
    error = False

    if request.user.is_authenticated:
        return redirect(reverse(connected))

    if request.method == "POST":
        form = forms.ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)  # Nous vérifions si les données sont correctes
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)  # nous connectons l'utilisateur
                return redirect(reverse(connected))
            else: # sinon une erreur sera affichée
                error = True

    return render(request, 'login.html', locals())

def deconnexion(request):
    logout(request)
    return redirect(reverse(connexion))